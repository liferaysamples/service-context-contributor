package space.manhart.demo.context.contributor;

import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.template.TemplateContextContributor;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;

/**
 * COMMENT: This class adds all neccessary variables into the freemarker context for the themes to use.
 * @author Manuel Manhart
 */
@Component(
	immediate = true,
	property = {"type=" + TemplateContextContributor.TYPE_THEME},
	service = TemplateContextContributor.class
)
public class ServiceContextContributor
	implements TemplateContextContributor {

	// COMMENT: Override and put everything needed into the context
	@Override
	public void prepare(
		Map<String, Object> contextObjects, HttpServletRequest request) {
		ServiceContext serviceContext = com.liferay.portal.kernel.service.ServiceContextThreadLocal.getServiceContext();
		contextObjects.put("serviceContext", serviceContext);
	}
}