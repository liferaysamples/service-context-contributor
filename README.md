# service-context-contributor

This sample shows how a to add / contribute variables into the freemarker context available in themes. It can be used in cases where themes cannot get certain liferay internal things.

## Getting Started

This project was generated with 

	blade create -b maven -t template-context-contributor servlet-context-contributor 

All comments in the code start with a ``COMMENT:`` so they can easily be found via search function.

### Prerequisites

* Running Liferay 7.1 CE
* Maven 3
* Java 8

### Installing

Build with

	# mvn clean package

Deploy it with

	# telnet 127.0.0.1 11311
	g! install file:///PATH_TO_FILE
	g! start BUNLDE_ID

Replace the variable ``PATH_TO_FILE`` with the absolute path to the jar file (in target folder of the project) and the ``BUNDLE_ID`` with the bundle id Gogo Shell returns when the bundle was successfully installed.

### Extended Files

* [init_custom.ftl](src/main/java/space/manhart/demo/context/contributor/ServiceContextContributor.java) - puts 8neccessary variables into the freemarker context 

## Built With

* [Blade](https://dev.liferay.com/develop/tutorials/-/knowledge_base/7-1/blade-cli) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [Liferay](https://dev.liferay.com/) - Portal / runtime container

## Further information

* [Template Context Contributor](https://dev.liferay.com/develop/reference/-/knowledge_base/7-1/template-context-contributor)

## Contributing

No contributing since this is a sample project.

## Versioning

No versioning since this is a sample project.

## Authors

* **Manuel Manhart** - *Initial work*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.
